/*
@licstart
Copyright (C) 2015-2016 Tails Developers

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally requestuired by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
@licend
*/

'use strict';

/*
 * To enable console logging in production set DEBUG to true
 */

var DEBUG = false;
if(typeof process !== 'undefined' && process.env.MIRROR_DEBUG === '1') {
 DEBUG = true;
}
// Allow debugging through the JS console
if(DEBUG === false) {
    var console = { log: function() {} };
}

/*
 * TEST_FILE should always be available, in any working mirror
 */

var TEST_FILE = 'project/trace';

var TEST_INITIAL_MIRROR_TIMEOUT = 1500; // milliseconds
var TEST_MIRROR_TIMEOUT = TEST_INITIAL_MIRROR_TIMEOUT;
var TEST_MAX_MIRROR_TIMEOUT = 40000;

var IN_BROWSER = (typeof process === 'undefined')

var ENFORCE_TLS = true; // setting this to false can be useful while debugging

/*
 * Some versions of IE do not support logging to the console.
 * When we set ALERTFALLBACK to true, we can log to alert.
 * In any case, if console function does not exist, we do not want to log
 * nor throw an error, thus disabling console.log.
 */

var ALERTFALLBACK = false;
if (typeof console === "undefined" || typeof console.log === "undefined") {
  console = {};
  if (ALERTFALLBACK) {
    console.log = function(msg) {
      alert(msg);
    };
  } else {
    console.log = function() {};
  }
  if(DEBUG && !IN_BROWSER) {
      console.log = function() {
          for(var i=0; i< arguments.length; i++) {
            process.stderr.write(arguments[i].toString())
            process.stderr.write(" ")
          }
          process.stderr.write("\n")
      }
  }
}
function get(path, callback) {

  var max_expected_filelength = 32 * Math.pow(2,10);

  // Do the usual request stuff
  var request = new XMLHttpRequest();
  request.open('GET', path);

  // Handle network errors
  request.onerror = function() {
    console.log("Network Error");
    return false;
  };

  request.onreadystatechange = function() {
    // Check the status
    // 4 = Headers and responseText have been loaded.
    if (request.readyState === 4) {
      // console.log(request.getAllResponseHeaders());
      // check filesize is not too big
      if(request.responseText.length > max_expected_filelength) {
        console.log("Retrieved content too big. Response length: " + request.responseText.length + " max_expected_filelength: " +  max_expected_filelength);
        request.abort();
        return false;
      }
      // Check TLS
      if(ENFORCE_TLS && !/^https:\/\//.test(request.responseURL)) {
        console.log("File not served over TLS.");
        request.abort();
        return false;
      }
      // empty string = text responseType
      if(request.responseType != "") {
        console.log("Expected responseType is text.");
        request.abort();
        return false;
      }

      // 200 = The request has succeeded.
      if (request.status === 200) {
        var data = request.responseText;
        console.log("data = request.responseText: " + data);
        if (callback) {
          callback(data);
        }
      } else {
        console.log(request.statusText);
        request.abort();
        return false;
      }
    }
  };

  // Make the request
  request.send();
}

/*
 * Choose a random mirror, based on weight.
 * weight="0" - this mirror is not active.
 * There is no max weight.
 * @params: JSON mirror data, formatted like this:
   - schema: https://git-tails.immerda.ch/mirror-pool/tree/schema.json
   - example: https://git-tails.immerda.ch/mirror-pool/tree/example-mirrors.json
 * @returns  false or one randomly chosen host URL prefix
 */
function pickRandomMirrorUrlPrefix(data) {
  if (data.mirrors.length > 0) {
    var end_of_last_range = 0,
    i,
    mirrors = [],
    picked_value,
    ranges_end = [];

    // Build a "compressed" version of the array that would contain N times each
    // mirror, with N being its weight. We "compress" it by only storing, for
    // each mirror, the index of the last element it would occupy in the
    // uncompressed version of the array.
    for (i = 0; i < data.mirrors.length; i++) {
      ranges_end[i]
      = end_of_last_range
      = end_of_last_range + data.mirrors[i].weight;
    }

    // If we were using the "uncompressed" version of the aforementioned array,
    // we would do something like Math.random() * uncompressed_array.length,
    // and the result would be the mirror we want. But here we are using
    // a "compressed" version of this array, so the result we get instead is
    // the minimum *weight* of the mirror we pick randomly.
    picked_value = Math.floor(Math.random() * end_of_last_range) + 1;

    // Pick the first mirror that would occupy a range that ends at or after
    // uncompressed_array[picked_value] in the "uncompressed" version
    // of the array. I.e. once converted to the "compressed" optimization:
    // the one whose ranges_end[i] is bigger or equal to picked_value.
    for (i = 0; i < data.mirrors.length; i++) {
      if (picked_value <= ranges_end[i]) {
        if(isValidURL(data.mirrors[i].url_prefix)) {
          return data.mirrors[i].url_prefix;
        }
      }
    }
  } else {
    return false;
  }
}

function isEmptyMirrorSet(data) {
  var sum = 0;
  for(var i=0; i<data.mirrors.length; i++) {
    sum = sum + data.mirrors[i].weight;
  }
  return sum === 0;
}

function removeMirrorFromData(prefix, data) {
  data.mirrors = data.mirrors.filter(function(mirror) {
    return mirror.url_prefix !== prefix
  })
}

function setWeight(prefix, data, weight) {
  data.mirrors = data.mirrors.map(function(mirror) {
    if (mirror.url_prefix !== prefix) return mirror;
    if (mirror.weight === weight) return mirror;
    // copy
    console.log("Changing " + prefix + " weight to ", weight);
    var new_mirror = {};
    for(var key in mirror) {
      new_mirror[key] = mirror[key];
    }
    new_mirror.weight = 1;
    return new_mirror;
  })
}

function testMirrorBrowser(prefix, on_error, on_success) {
  // We can't check, because CORS
  on_success()
}
function testMirrorNode(prefix, on_error, on_success) {
  var url = prefix + TEST_FILE;
  if (DEBUG && prefix.indexOf('httpstat.us') !== -1) {
    url = prefix
  }
  var request_options = {
    uri: url,
    method: 'HEAD',
    timeout: TEST_MIRROR_TIMEOUT,
  }
  TEST_MIRROR_TIMEOUT = Math.min(
    TEST_MIRROR_TIMEOUT * 2,
    TEST_MAX_MIRROR_TIMEOUT,
    )
  require('request')(
    request_options
  ).on('error', function onError(err) {
    on_error(false)
  }).on('response', function onResponse(response) {
    if(response.statusCode === 200) {
      on_success()
    } else {
      on_error(true)
    }
  })
}

function testMirror(prefix, onError, onSuccess) {
  if(IN_BROWSER) {
    testMirrorBrowser(prefix, onError, onSuccess)
  } else {
    testMirrorNode(prefix, onError, onSuccess)
  }
}

/*
 * Gets a working random mirror, and call the appropriate callback then
 */
function getRandomMirrorUrlPrefix(data, max_retries, onSuccess, onFailure) {
  if(max_retries === 0) {
    onFailure("max retries exceeded")
    return
  }
  if(isEmptyMirrorSet(data)) {
    onFailure("no mirror seems to be working")
    return
  }
  var prefix = pickRandomMirrorUrlPrefix(data)
  testMirror(prefix, function retryOnError(shouldRemove) {
    if(shouldRemove) {
      // when the mirror is reporting an error (ie: 404 or 500), let's remove it from the list
      removeMirrorFromData(prefix, data)
    } else {
      // when we have a connection error (ie: timeout), let'se reduce its weight, but not remove it
      // completely. this still allows recovering from user's network failures, but makes it less probable
      // that the user is constantly checking the same mirror
      setWeight(prefix, data, 1)
    }
    getRandomMirrorUrlPrefix(data, max_retries - 1, onSuccess, onFailure)
  }, function mirrorWorks() {
    onSuccess(prefix)
  })
}

/*
 * Returns true if url is a valid URL pattern
 */
function isValidURL(url) {
  var url_pattern = new RegExp('^(http|https):\/\/[a-z0-9\-_]+(\.[a-z0-9\-_]+)+([a-z0-9\-_\.,@\?^=%&;:/~\+#]*[a-z0-9\-\_#@\?^=%&;/~\+])?$', 'i');
  var test = url_pattern.test(url)
  console.log("Tested URL: " + url + " " + test);
  return(test);
}

/*
 * This is the main function call, to be called by our website
 */
function replaceUrlPrefixWithRandomMirror(linkelements) {
  var data, index, max_expected_linkelements, url_current, fallback_download_url_prefix, url_new, url_prefix;

  max_expected_linkelements = 15;
  if(linkelements.length > max_expected_linkelements) {
    console.log(linkelements.length + " exceeds number of expected elements: " + max_expected_linkelements);
    return false;
  }

  fallback_download_url_prefix = "http://dl.amnesia.boum.org/tails/";

  return get('/mirrors.json', function(data){
     if( data !== "undefined" ) {
       data = JSON.parse(data);

       // pick a random mirror
       getRandomMirrorUrlPrefix(data, 20, function gotPrefix(url_prefix) {
         console.log("Random URL prefix: " + url_prefix);

         // replace the default prefix with the newly picked one,
         // in the href of each linkelement
         for (index = 0; index < linkelements.length; index++) {
           // remove possible URL whitespaces and line breaks as created by ikiwiki
           url_current = linkelements[index].getAttribute('href');
           url_current = url_current.trim();
           if (url_current !== 'undefined' && isValidURL(url_current)) {
             url_new = url_prefix +
               url_current.slice(fallback_download_url_prefix.length);
             console.log("index: "+ index + " url_current: " + url_current + " --> url_new:" + url_new);
             linkelements[index].setAttribute('href', url_new)
           } else {
             console.log(url_current + " appears to be malformed.");
           }
         }
       })
    }
  });
}

/* This function is used by bin/tails-transform-mirror-url.
 * Use mirror pool configuration and select a random mirror.
 */
function transformURL(url, fallback_download_url_prefix, mirrors, onSuccess, onFailure) {
  var prefix = getRandomMirrorUrlPrefix(mirrors, 20, function(prefix) {
    var candidateUrl = prefix + url.slice(fallback_download_url_prefix.length);
    onSuccess(candidateUrl)
  },
    onFailure)
}

/*
 * Assign transformURL to exports, so that Tails Upgrader can use
 * it: it runs bin/tails-transform-mirror-url with nodejs.
 * Will return target object.
 */

if(!IN_BROWSER) {
  module.exports.transformURL = transformURL;
}

#!/usr/bin/env python3
'''
This script is useful to get a "real" mirrors.json file, and split it into many mirrors-XXX.json files, each
of which contains a single mirror.

This is useful for debugging, for example::

    ./split-mirrors.py ../mirrors.json .
    for mirror in mir*.json; do
        env NODE_PATH=lib/js/ \
        ./bin/tails-transform-mirror-url \
        'http://dl.amnesia.boum.org/tails/stable/tails-amd64-4.21/tails-amd64-4.21.img' \
        http://dl.amnesia.boum.org/ \
        "$(cat "$mirror" )"
    done
'''

import os.path
import json
import sys

def main():
    fname = sys.argv[1]
    destdir = sys.argv[2]
    data = json.load(open(fname))
    mirrors = data['mirrors']
    other_data = {k: data[k] for k in data if k != 'mirrors'}

    for i, mirror in enumerate(mirrors):
        if mirror.get('weight', 0) == 0:
            continue
        with open(os.path.join(destdir, 'mirror-%03d.json' % i), 'w') as buf:
            mirror_data = dict(mirrors=[mirror], **other_data)
            json.dump(mirror_data, buf, indent=2)



if __name__ == '__main__':
    main()
